import * as AWS from 'aws-sdk';

AWS.config.update({ region: process.env.REGION });

const isEmptyString = (str) => typeof str != 'string' || str.trim() === '';

const getUserByUserName = (dynamoDb, userName) => {
  return new Promise((resolve, reject) => {
    if (isEmptyString(userName)) {
      reject('Username must be provided');
    }

    try {
      dynamoDb.getItem({
        TableName: 'users',
        Key: {
          email: { S: userName },
        }
      }, (error, data) => {
        if (error) {
          reject(error);
        } else {
          resolve(data.Item);
        }
      });
    } catch (dynamoDbError) {
      reject(dynamoDbError);
    }
  });
}

const addUser = (dynamoDb, userName) => {
  return new Promise((resolve, reject) => {
    if (isEmptyString(userName)) {
      reject('Username must be provided');
    }

    try {
      dynamoDb.putItem(
        {
          TableName: 'users',
          Item: {
            email: { S: userName },
            isRegistrationComplete: { BOOL: false },
          }
        },
        (error, data) => {
          if (error) {
            reject(error);
          } else {
            resolve(data);
          }
        }
      );
    } catch (dynamoDbError) {
      reject(dynamoDbError);
    }
  });
}

export const handler = async (event, _, callback) => {
  try {
    const dynamoDb = new AWS.DynamoDB({ apiVersion: '2012-10-08' });
    const userName = event.userName;
    const user = await getUserByUserName(dynamoDb, userName);

    if (user) {
      throw `User with the given username already exists. Username: ${userName}`;
    } else {
      await addUser(dynamoDb, userName);
      callback(null, event);
    }
  } catch (addUserError) {
    callback(addUserError);
  }
};
