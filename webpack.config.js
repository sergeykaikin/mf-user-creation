const path = require('path');
const slsw = require('serverless-webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: slsw.lib.entries,
    resolve: {
        extensions: ['.js', '.json']
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js'
    },
    target: 'node',
    stats: 'minimal',
    devtool: 'source-map',
    plugins: getPlugins()
};

function getPlugins() {
    const defaultPlugins = [];
    if (process.env.NODE_ENV === 'production') {
        return defaultPlugins.concat([
            new UglifyJSPlugin({
                sourceMap: true
            })
        ]);
    } else {
        return defaultPlugins;
    }
}
